;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Mahesh Padmanabhan"
      user-mail-address "sutram@gmail.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Dropbox/Org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

(add-to-list 'initial-frame-alist '(fullscreen . maximized))

(global-undo-tree-mode)

(remove-hook 'doom-first-buffer-hook #'global-hl-line-mode)

(custom-set-faces!
  `(font-lock-comment-face :foreground ,(doom-lighten 'grey .25))
  `( aw-leading-char-face
    :foreground "white" :background "red"
    :weight bold :height 2.5 :box (:line-width 10 :color "red") )
  `(font-lock-doc-face     :foreground ,(doom-lighten 'grey .25))
  `(hl-line                :forground nil :background "#3d4753")
  `(region                 :foreground "#000000" :background ,(doom-lighten 'grey 0.3))
  `(line-number            :foreground ,(doom-lighten 'grey .3)))

;; Change font size between desktop and laptop
(if (string-equal "vayu" (system-name))
    (setq doom-font (font-spec :family "Ubuntu Mono" :size 17 :weight 'regular)
          doom-variable-pitch-font (font-spec :family "Ubuntu Mono" :size 17))
  (setq doom-font (font-spec :family "Ubuntu Mono" :size 20 :weight 'regular)
        doom-variable-pitch-font (font-spec :family "Ubuntu Mono" :size 20)))

;; Prevents some cases of Emacs flickering
(add-to-list 'default-frame-alist '(inhibit-double-buffering . t))

(setq!
 avy-all-windows t
 which-key-idle-delay 0.4
 confirm-kill-emacs nil
 bookmark-default-file (concat doom-private-dir "bookmarks")
 undo-fu-session-directory (concat doom-private-dir "undo-fu-session/")
 evil-vsplit-window-right t
 evil-split-window-below t
 lsp-enable-file-watchers nil
 which-key-allow-multiple-replacements t)

(after! recentf
  (setq recentf-save-file (concat doom-private-dir "recentf")))

;; Shorten which-key item names
(after! which-key
  (pushnew!
   which-key-replacement-alist
   '(("" . "\\`+?evil[-:]?\\(?:a-\\)?\\(.*\\)") . (nil . "◂\\1"))
   '(("\\`g s" . "\\`evilem--?motion-\\(.*\\)") . (nil . "◃\\1"))
   ))

;; Preview window to place after split
(after! ivy
  (defadvice! prompt-for-buffer (&rest _)
    :after '(evil-window-split evil-window-vsplit)
    (+ivy/switch-buffer))

  (setq +ivy-buffer-preview t))

;; Winum like spacemacs
(after! (winum which-key)
  (push '((nil . "winum-select-window-[0-9]") . t) which-key-replacement-alist))

(after! winum
  (setq winum-scope 'frame-local))
;; (after! lsp-ui
;;   (add-hook! 'lsp-ui-mode-hook
;;     (run-hooks (intern (format "%s-lsp-ui-hook" major-mode)))))

;; (require flycheck-pyflakes)
;; (defun my-python-flycheck-setup ()
;;     (flycheck-add-next-checker 'lsp 'python-pylint))

;; (add-hook 'python-mode-lsp-ui-hook #'my-python-flycheck-setup)

(map!
 ;; :n "TAB" #'indent-for-tab-command
 (:after winum :leader
  :n "0" #'winum-select-window-0-or-10
  :n "1" #'winum-select-window-1
  :n "2" #'winum-select-window-2
  :n "3" #'winum-select-window-3
  :n "4" #'winum-select-window-4
  :n "5" #'winum-select-window-5
  :n "6" #'winum-select-window-6
  :n "7" #'winum-select-window-7
  :n "8" #'winum-select-window-8
  :n "9" #'winum-select-window-9)

 (:leader
  (:desc "Split and indent" :n  "i n" #'sp-newline)

  (:prefix-map ("a" . "applications")
   (:prefix ("p" . "pianobar")
    :desc "play-or-pause" "=" #'pianobar-play-or-pause
    :desc "start" "p" #'pianobar)
   )

  ;; Use smart-parens to deal with parens/brackets etc. in non-lisp mode (from Spacemacs)
  (:prefix-map ("k" . "lisp state")
   "s" #'sp-forward-slurp-sexp
   "S" #'sp-backward-slurp-sexp
   "w" #'lisp-state-wrap
   "W" #'sp-unwrap-sexp
   "b" #'sp-forward-barf-sexp
   "B" #'sp-backward-barf-sexp
   "h" #'sp-backward-symbol
   "H" #'sp-backward-sexp
   "l" #'sp-forward-symbol
   "L" #'sp-forward-sexp
   "ds" #'sp-kill-symbol
   "Ds" #'sp-backward-kill-symbol
   "dw" #'sp-kill-word
   "Dw" #'sp-backward-kill-word
   "dx" #'sp-kill-sexp
   "Dx" #'sp-backward-kill-sexp
   )))

(use-package! alert
  :defer t
  :config
  (setq alert-default-style
        (pcase system-type
              ('gnu/linux 'libnotify)
              ('darwin 'notifier)
              ('windows-nt 'toaster)
              (_ 'notifier)))
  (setq alert-fade-time 7))

(eval-after-load 'pdf-tools
  (progn
    ;; So that Emacs doesn't warn about large pdf files
    (defadvice! fix-abort-if-file-too-large-a (orig-fn size op-type filename &optional offer-raw)
      :around #'abort-if-file-too-large
      (unless (string-match-p "\\.pdf\\'" filename)
        (funcall orig-fn size op-type filename offer-raw)))

    ;; So that reading a bookmarked PDF location loads properly
    (defadvice! fix-pdf-load-from-bm-a (orig-fn bmk)
      :around #'pdf-view-bookmark-jump-handler
      (pdf-tools-install t t t nil)
      (funcall orig-fn bmk))))

(load! "elisp/my-org")
(load! "elisp/my-vterm")
(load! "elisp/my-pianobar")
