;;; elisp/my-vterm.el -*- lexical-binding: t; -*-

(global-set-key (kbd "C-x C-1") 'pmi/named-term)
(global-set-key (kbd "C-x C-2") 'vterm-toggle)

(defun pmi/named-term (term-name)
  "Generate a terminal with buffer name TERM-NAME."
  (interactive "sTerminal purpose: ")
  (vterm (concat "term-" term-name)))

(after! vterm ; when installing, evaluate exec-path first (else 'command not found')
  ;; (setq vterm-buffer-name-string "vterm %s")

  (evil-define-key '(normal insert) vterm-mode-map (kbd "C-a")      #'vterm--self-insert)
  (evil-define-key '(normal insert) vterm-mode-map (kbd "C-e")      #'vterm--self-insert)
  (evil-define-key '(normal insert) vterm-mode-map (kbd "C-k")      #'vterm--self-insert)
  (evil-define-key '(normal insert) vterm-mode-map (kbd "C-r")      #'vterm--self-insert)
  (evil-define-key '(normal insert) vterm-mode-map (kbd "C-p")      #'vterm--self-insert)
  (evil-define-key '(normal insert) vterm-mode-map (kbd "C-n")      #'vterm--self-insert)
  (evil-define-key '(normal insert) vterm-mode-map (kbd "C-c")      #'vterm--self-insert)
  (evil-define-key '(normal insert) vterm-mode-map (kbd "C-d")      #'vterm--self-insert)
  (evil-define-key '(normal) vterm-mode-map (kbd "p")               #'vterm-yank)
  (evil-define-key '(normal) vterm-mode-map (kbd "P")               #'(lambda ()
                                                                        (interactive)
                                                                        (vterm-send-C-b)
                                                                        (vterm-yank)))
  (evil-define-key '(normal insert) vterm-mode-map (kbd "<C-backspace>")
    #'vterm-send-meta-backspace)
  )

(use-package! vterm-toggle
  :after ( vterm evil)
  ;; :load-path "~/.emacs.d/private"
  :config
  (setq vterm-toggle-fullscreen-p nil)
  (with-eval-after-load 'evil
    (evil-set-initial-state 'vterm-mode 'insert))
  ;; (spacemacs/set-leader-keys "ovd" 'vterm-toggle-cd)
  ;; Switch to next vterm buffer
  (evil-define-key '(normal insert) vterm-mode-map (kbd "C-x C-4")  'vterm-toggle-forward)
  (evil-define-key '(normal insert) vterm-mode-map (kbd "C-x C-5")  'vterm-toggle-backward)
  ;; (add-to-list 'display-buffer-alist
  ;;              '("^v?term.*"
  ;;                (display-buffer-reuse-window display-buffer-at-bottom)
  ;;                (reusable-frames . visible)
  ;;                (window-height . 0.5)))
  )

(provide 'my-vterm)
