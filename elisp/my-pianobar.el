;;; elisp/my-pianobar.el -*- lexical-binding: t; -*-

(after! pianobar
  (setq pianobar-username (+pass-get-secret "Pandora/user-name"))
  (setq pianobar-password (+pass-get-secret "Pandora/password"))
  ;; Setup pianobar faces to follow the color scheme
  (set-face-foreground 'pianobar-mode-input-face (face-foreground 'font-lock-variable-name-face))
  (set-face-foreground 'pianobar-mode-time-face  (face-foreground 'font-lock-constant-face))
  (set-face-foreground 'pianobar-mode-info-face  (face-foreground 'font-lock-comment-face))
  (set-face-foreground 'pianobar-mode-prompt-face (face-foreground 'font-lock-keyword-face))


  ;; Use x to quit the pianobar window
  (define-key pianobar-mode-map (kbd "x")
    (lambda (N)
      (interactive "p")
      ;; Check if pianobar is prompting, so you can still type "q" into prompts
      (if pianobar-is-prompting
          (self-insert-command N)
        (quit-window)))))

(provide 'my-pianobar)
